<!DOCTYPE html>
<head>
	 <title>Search Box</title>
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">


<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	 <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
 

 <div class="container">
    <br>
    <br>
    <div class="row">
      <div class="col-4">
       <input type="text" id="search-box" name="SearchBox" class="form-control">
       <div id="suggestionlist"></div>
  
      </div>
      <div class="col-2">
        <button id="addbook" class="btn btn-primary">Search</button>
      </div>
    </div>
 	
 	
 	
 </div>


</body>
</html>


<script type="text/javascript">

	$( function() {
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		$("#search-box").autocomplete({
			source: function( request, response ) { 
           $.ajax({
            url:"searchbook",
            type: 'post',
            dataType: "json",
            data: {
               _token: CSRF_TOKEN,
               search: request.term
            },
            success: function( data ) {
            	$( "#suggestionlist" ).empty();
           for (var i = 0; i < data.length; i++) {
                 
                content = "<ul class='list-group'><li class='list-group-item'>"+ data[i].name+ "</li></ul>";
                $(content).appendTo("#suggestionlist"); 
              
            }
           }
         });

         
         },
        select: function (event, ui) {
           $('#search-box').val(ui.item.name); 
           return false;
        }
      });

			$("#addbook").click(function(){
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
       $.ajax({
       	url:"addbook",
       	type: "post",
       	dataType: "JSON",
       	  data: {
               _token: CSRF_TOKEN,
               addbook: $("#search-box").val(),
              
            },
             success: function( response ) {
                      
                      window.location.href= '/'; 
             }

       })
	});

	});
	
</script>