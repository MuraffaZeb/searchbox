<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\books;

class booklistController extends Controller
{
    public function allbooks()
    {
    	$all_books= books::all();

    	return view('addbook', compact('all_books'));
    }

    public function addbook( Request $request)
    {
    	$new_book= $request->addbook;

    	$books=books::firstOrCreate(['bookName'=> $new_book]);

    	return $books;
            
    }
}
