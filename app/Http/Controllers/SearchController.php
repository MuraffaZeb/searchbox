<?php

namespace App\Http\Controllers;
use App\books;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    //
    public function getbooks(Request $request){
          $search_input = $request->search;
          
    	
          if($search_input == "")
          {
          	$books= books::select('id','bookName')->orderby('id')->get();
          	
          }
          else
          {
          	$books= books::select('id','bookName')->where('bookName' ,"LIKE", $search_input . '%')->orderby('id')->get();
             
          }
    	
    	 $response = array();
         foreach($books as $book){
         $response[] = array( "value"=> $book->id,"name"=>$book->bookName);
         }
         
          return  response()->json($response);
        
       
         
        
    }
}
