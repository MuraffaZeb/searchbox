<?php

use Illuminate\Database\Seeder;

class books extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('books')->insert(
      
       [
       	'bookName'=> 'Norway Diary'
       ]);
      
    }
}
